# GITLab CI-CD Workshop:
complete devops platform
---
**CI**: continuous integration.
**CD**: continuous delivery/deployment
---
`Basic means: code changes -> Test -> Build -> Release`
---
1. Architecture:
2. Pipeline.
3. Gitlab runners.
4. It is use docker. docker images contain all data.
---
**.gitlab-ci.yml -> create this file -> will automatically run pipeline**
---
* Job: What to do.
* Script: Commands to execute.
* before,after script.
---
* Cache
* test -d vendor ||
---

##### Command for PHPUnit:

`vendor/bin/phpunit --bootstrap src/autoload.php --colors=never --log-junit phpunit-report.xml tests
`
